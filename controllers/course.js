//Dito ilalagay ang business logic kay course

const Course = require("../models/Course")



module.exports.addCourse = (reqBody, userData) => {

    return Course.findById(userData.userId).then(result => {

        if (userData.isAdmin == false) {
            return "You are not an admin"
        } else {
            let newCourse = new Course({
                name: reqBody.name,
                description: reqBody.description,
                price: reqBody.price
            })
        
            //Saves the created object to the database
            return newCourse.save().then((course, error) => {
                //Course creation failed
                if(error) {
                    return false
                } else {
                    //course creation successful
                    return "Course creation successful"
                }
            })
        }
        
    });    
}


//Something wrong with syntax



//retrieve all courses
module.exports.getAllCourses = () => {
	return Course.find({}).then(result => {
		return result;
	})
}



//Retrieve all active courses
module.exports.getAllActive = () => {
	return Course.find({isActive: true}).then(result => {
		return result
	})
}

//Retrieve a specific course

module.exports.getCourse = (reqParams) => {
	return Course.findById(reqParams.courseId).then(result => {
		return result;
	})
}

//Update a course
module.exports.updateCourse = (reqParams, reqBody) => {
	let updatedCourse = {
		name: reqBody.name,
		description: reqBody.description,
		price: reqBody.price
	}
//find by id and update
return Course.findByIdAndUpdate(reqParams.courseId, updatedCourse).then((course, error) => {
	if(error){
		return false;
	}else{
		return true
	}
})
}


//activity 35


/*module.exports.archieveCourse = (reqParams, reqBody) => {
	let archievedCourse = {
		isActive: reqBody.isActive
	}
//find by id and update
return Course.findByIdAndArchieve(reqParams.courseId, archievedCourse).then((course, error) => {
	if(error){
		return false;
	}else{
		return true
	}
})
}*/

module.exports.archieveCourse = (reqParams, reqBody) => {
	let archievedCourse = {
		isActive: reqBody.isActive
	}
//find by id and update
return Course.findByIdAndUpdate(reqParams.courseId, archievedCourse).then((course, error) => {
	if(error){
		return false;
	}else{
		return true
	}
})
}


















